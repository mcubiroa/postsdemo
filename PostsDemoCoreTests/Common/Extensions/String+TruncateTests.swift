//
//  String+TruncateTests.swift
//  PostsDemoCoreTests
//
//  Created by Marc Cubiro Aguilar on 01/02/2020.
//  Copyright © 2020 marc.cubiro. All rights reserved.
//

import XCTest
@testable import PostsDemoCore

class String_TruncateTests: XCTestCase {

    func testTruncateLongString() {
        var string = "sunt aut facere repellat provident occaecati"
        string = string.truncate(length: 10)
        XCTAssertEqual(string, "sunt aut f...")
    }

    func testTruncateShortSting() {
        var string = "sunt"
        string = string.truncate(length: 10)
        XCTAssertEqual(string, "sunt")
    }

}
