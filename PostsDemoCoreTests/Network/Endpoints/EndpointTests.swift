//
//  EndpointTests.swift
//  PostsDemoCoreTests
//
//  Created by Marc Cubiro Aguilar on 01/02/2020.
//  Copyright © 2020 marc.cubiro. All rights reserved.
//

import XCTest
@testable import PostsDemoCore

class EndpointTests: XCTestCase {

    var mockURL = URL(string: "www.test.com/")!

    func testPostsRequest() {
        let endpoint = Endpoint.posts
        let request = endpoint.request(with: mockURL)
        
        XCTAssertEqual(request.url, URL(string: "www.test.com/" + endpoint.path))
        XCTAssertEqual(request.httpMethod, endpoint.httpMehtod.rawValue)
    }
    
    func testCommentsRequest() {
        let endpoint = Endpoint.comments(postId: 0)
        let request = endpoint.request(with: mockURL)
        
        XCTAssertEqual(request.url, URL(string: "www.test.com/" + endpoint.path + "?postId=0"))
        XCTAssertEqual(request.httpMethod, endpoint.httpMehtod.rawValue)
    }
}
