//
//  PostDetailRepositoryTests.swift
//  PostsDemoCoreTests
//
//  Created by Marc Cubiro Aguilar on 01/02/2020.
//  Copyright © 2020 marc.cubiro. All rights reserved.
//

import XCTest
@testable import PostsDemoCore

class PostDetailRepositoryTests: XCTestCase {

    var session: URLSessionMock!
    var webService : WebService!
    var postDetailRepository: PostDetailRepository!
    let testURL = URL(string: "www.tets.com")!
    let timeout = 0.5
    
    override func setUp() {
        super.setUp()

        session = URLSessionMock()
        webService = WebService(session: session)
        postDetailRepository = PostDetailRepository(webService: webService)
    }

    func testFetchPostsSuccess() {
        let exp = expectation(description: "Fetching posts")
        let responseMock = HTTPURLResponse(url: testURL, statusCode: 200, httpVersion: nil, headerFields: nil)
        session.response = responseMock
        session.data = [CommentResponse(postId: 0, id: 0, name: "", email: "", body: "")].toData()
        
        var success = false
        postDetailRepository.fetchComments(postId: 0){ (response) in
            switch response {
            case .success:
                success = true
            case .failure:
                success = false
            }
            exp.fulfill()
        }
        waitForExpectations(timeout: timeout)
        XCTAssertEqual(success, true)
    }

    func testFetchPostsFailure() {
        let exp = expectation(description: "Fetching posts")
        let responseMock = HTTPURLResponse(url: testURL, statusCode: 200, httpVersion: nil, headerFields: nil)
        session.response = responseMock
        session.data = Data()
        
        var success = false
        postDetailRepository.fetchComments(postId: 0) { (response) in
            switch response {
            case .success:
                success = true
            case .failure:
                success = false
            }
            exp.fulfill()
        }
        waitForExpectations(timeout: timeout)
        XCTAssertEqual(success, false)
    }

}
