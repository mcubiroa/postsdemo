//
//  PostDetailPresenterTests.swift
//  PostsDemoCoreTests
//
//  Created by Marc Cubiro Aguilar on 01/02/2020.
//  Copyright © 2020 marc.cubiro. All rights reserved.
//

import XCTest
@testable import PostsDemoCore

fileprivate class PostDetailViewMock: PostDetailViewProtocol {
    var showPostCalled = false
    var showComentsCalled = false
    var showErrorCalled = false
    
    func showPost(post: PostDetail) {
        showPostCalled = true
    }
    
    func showListOfComments(_ comments: [Comment]) {
        showComentsCalled = true
    }
    func showError(error: ServiceError) {
        showErrorCalled = true
    }
}

fileprivate class PostDetailRepositoryMock: PostDetailRespositoryProtocol {
    var fetchComentsCalled = false
    var fetchCommentsSuccess = false
    
    func fetchComments(postId: Int, completion: @escaping (Result<[CommentResponse], ServiceError>) -> Void) {
        fetchComentsCalled = true
        if fetchCommentsSuccess {
            completion(.success([]))
        } else {
            completion(.failure(.unexpected))
        }
    }
}

class PostDetailPresenterTests: XCTestCase {

    fileprivate var repository: PostDetailRepositoryMock!
    fileprivate var view: PostDetailViewMock!
    var presenter: PostDetailPresenter!

    override func setUp() {
        view = PostDetailViewMock()
        repository = PostDetailRepositoryMock()
        presenter = PostDetailPresenter(repository: repository, post: Post(id: 0, userId: 0, title: "", bodyPreview: "", body: ""))
        presenter.view = view
    }

    func testFetchCommentsSuccess() {
        repository.fetchCommentsSuccess = true
        presenter.fetchComments()
        
        XCTAssertEqual(repository.fetchComentsCalled, true)
        XCTAssertEqual(view.showComentsCalled, true)
    }
    
    func testFetchPostsFail() {
        repository.fetchCommentsSuccess = false
        presenter.fetchComments()
        
        XCTAssertEqual(repository.fetchComentsCalled, true)
        XCTAssertEqual(view.showErrorCalled, true)
    }
    
    func testGetComments() {
        let expectedComments = [Comment(id: 0, name: "A", email: "B", body: "C")]
        let commentsResponse = [CommentResponse(postId: 0, id: 0, name: "A", email: "B", body: "C")]
        
        let comments = presenter.getComments(from: commentsResponse)
        
        XCTAssertEqual(comments, expectedComments)
    }
    
    func testGetPostDetail() {
        let post = Post(id: 0, userId: 0, title: "A", bodyPreview: "B", body: "B")
        let expectedPostDetail = PostDetail(id: 0, userId: 0, title: "A", body: "B", comments: [])
        let postDetail = presenter.getPostDetail(from: post)
        
        XCTAssertEqual(postDetail, expectedPostDetail)
    }
    
    func testLoadView() {
        presenter.loadView()
        XCTAssertEqual(view.showPostCalled, true)
    }
    

}
