//
//  PostsListRepositoryTests.swift
//  PostsDemoCoreTests
//
//  Created by Marc Cubiro Aguilar on 01/02/2020.
//  Copyright © 2020 marc.cubiro. All rights reserved.
//

import XCTest
@testable import PostsDemoCore

class PostsListRepositoryTests: XCTestCase {
    
    var session: URLSessionMock!
    var webService : WebService!
    var postsListRepository: PostsListRepository!
    let testURL = URL(string: "www.tets.com")!
    let timeout = 0.5
    
    override func setUp() {
        super.setUp()

        session = URLSessionMock()
        webService = WebService(session: session)
        postsListRepository = PostsListRepository(webService: webService)
    }

    func testFetchPostsSuccess() {
        let exp = expectation(description: "Fetching posts")
        let responseMock = HTTPURLResponse(url: testURL, statusCode: 200, httpVersion: nil, headerFields: nil)
        session.response = responseMock
        session.data = [PostsResponse(id: 0, userId: 0, title: "", body: "")].toData()
        
        var success = false
        postsListRepository.fetchPosts { (response) in
            switch response {
            case .success:
                success = true
            case .failure:
                success = false
            }
            exp.fulfill()
        }
        waitForExpectations(timeout: timeout)
        XCTAssertEqual(success, true)
    }

    func testFetchPostsFailure() {
        let exp = expectation(description: "Fetching posts")
        let responseMock = HTTPURLResponse(url: testURL, statusCode: 200, httpVersion: nil, headerFields: nil)
        session.response = responseMock
        session.data = Data()
        
        var success = false
        postsListRepository.fetchPosts { (response) in
            switch response {
            case .success:
                success = true
            case .failure:
                success = false
            }
            exp.fulfill()
        }
        waitForExpectations(timeout: timeout)
        XCTAssertEqual(success, false)
    }

}
