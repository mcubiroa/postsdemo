//
//  PostsListPresenterTests.swift
//  PostsDemoCoreTests
//
//  Created by Marc Cubiro Aguilar on 01/02/2020.
//  Copyright © 2020 marc.cubiro. All rights reserved.
//

import XCTest
@testable import PostsDemoCore

fileprivate class PostsListViewMock: PostsListViewProtocol {
    var showListOfPostsCalled = false
    var showErrorCalled = false
    
    func showListOfPosts(_ posts: [Post]) {
        showListOfPostsCalled = true
    }
    
    func showError(error: ServiceError) {
        showErrorCalled = true
    }
}

fileprivate class PostsListRepositoryMock: PostsListRespositoryProtocol {
    var fetchPostsIsCalled = false
    var fetchPostSuccess = false
    
    func fetchPosts(completion: @escaping (Result<[PostsResponse], ServiceError>) -> Void) {
        fetchPostsIsCalled = true
        if fetchPostSuccess {
            completion(.success([]))
        } else {
            completion(.failure(.unexpected))
        }
    }
}

class PostsListPresenterTests: XCTestCase {
    
    fileprivate var repository: PostsListRepositoryMock!
    fileprivate var view: PostsListViewMock!
    var presenter: PostsListPresenter!

    override func setUp() {
        view = PostsListViewMock()
        repository = PostsListRepositoryMock()
        presenter = PostsListPresenter(repository: repository)
        presenter.view = view
    }

    func testFetchPostsSuccess() {
        repository.fetchPostSuccess = true
        presenter.fetchPosts()
        
        XCTAssertEqual(repository.fetchPostsIsCalled, true)
        XCTAssertEqual(view.showListOfPostsCalled, true)
    }
    
    func testFetchPostsFail() {
        repository.fetchPostSuccess = false
        presenter.fetchPosts()
        
        XCTAssertEqual(repository.fetchPostsIsCalled, true)
        XCTAssertEqual(view.showErrorCalled, true)
    }
    
    func testGetPosts() {
        let expectedPosts = [Post(id: 0, userId: 0, title: "A", bodyPreview: "B", body: "B")]
        let postsResponse = [PostsResponse(id: 0, userId: 0, title: "A", body: "B")]
        
        let posts = presenter.getPosts(from: postsResponse)
        
        XCTAssertEqual(posts, expectedPosts)
    }

}
