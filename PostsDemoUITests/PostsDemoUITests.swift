//
//  PostsDemoUITests.swift
//  PostsDemoUITests
//
//  Created by Marc Cubiro Aguilar on 31/01/2020.
//  Copyright © 2020 marc.cubiro. All rights reserved.
//

import XCTest

class PostsDemoUITests: XCTestCase {

    let timeout: TimeInterval = 10
    var app: XCUIApplication!
    
    override func setUp() {
        app = XCUIApplication()
        continueAfterFailure = false
    }

    func testLaunchOnPostList() {
        app.launch()
    
        XCTAssertTrue(app.otherElements["PostListView"].waitForExistence(timeout: timeout))
    }

    func testGoToPostDetail() {
        
        app.launch()
        let cell = app.tables["PostsTableView"].cells.element(boundBy: 1)
        
        cell.tap()
        
        XCTAssertTrue(app.otherElements["PostDetailView"].waitForExistence(timeout: timeout))
    }
}
