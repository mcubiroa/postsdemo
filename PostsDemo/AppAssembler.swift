//
//  AppAssembler.swift
//  PostsDemo
//
//  Created by Marc Cubiro Aguilar on 02/02/2020.
//  Copyright © 2020 marc.cubiro. All rights reserved.
//

import UIKit
import PostsDemoCore

public final class AppAssembler {
    private var navigationController = UINavigationController()
    private var webService = WebServiceAssembler().webService
    private lazy var postsListAssembler = PostsListAssembler(webService: webService, navigationController: navigationController)
    
    public func viewController() -> UIViewController{
        navigationController.viewControllers = [postsListAssembler.viewController()]
        return navigationController
    }
}
