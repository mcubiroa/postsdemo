//
//  AppDelegate.swift
//  PostsDemo
//
//  Created by Marc Cubiro Aguilar on 31/01/2020.
//  Copyright © 2020 marc.cubiro. All rights reserved.
//

import UIKit
import PostsDemoCore

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.rootViewController = AppAssembler().viewController()
        window?.makeKeyAndVisible()
        
        return true
    }

}

