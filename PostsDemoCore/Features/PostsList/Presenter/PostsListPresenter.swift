//
//  PostsListPresenter.swift
//  PostsDemoCore
//
//  Created by Marc Cubiro Aguilar on 31/01/2020.
//  Copyright © 2020 marc.cubiro. All rights reserved.
//

import Foundation

// MARK: - PostsListPresenterProtocol
internal protocol PostsListPresenterProtocol {
    var view: PostsListViewProtocol? { get set }
    
    func loadView()
    func retry()
    func postSelected(post: Post)
}

// MARK: - PostsListPresenter
internal final class PostsListPresenter: PostsListPresenterProtocol {
    
    // MARK: - Properties
    let previewLenght = 80
    
    // MARK: - Variables
    var detailAssembler: PostDetailAssembler
    var repository: PostsListRespositoryProtocol
    weak var view: PostsListViewProtocol?

    // MARK: - Initialization
    init(repository: PostsListRespositoryProtocol, detailAssembler: PostDetailAssembler) {
        self.repository = repository
        self.detailAssembler = detailAssembler
    }
    
    // MARK: - Internal methods
    internal func loadView() {
        fetchPosts()
    }
    
    internal func retry() {
        fetchPosts()
    }
    
    internal func postSelected(post: Post) {
        detailAssembler.navigateToPostDetail(post: post)
    }
    
    internal func fetchPosts() {
        repository.fetchPosts { (response) in
            switch response {
            case .success(let postsResponseList):
                self.view?.showListOfPosts(self.getPosts(from: postsResponseList))
            case .failure(let error):
                self.view?.showError(error: error)
            }
        }
    }
    
    internal func getPosts(from postResponse: [PostsResponse]) -> [Post] {
        return postResponse.map { (post) -> Post in
            return Post(
                id: post.id,
                userId: post.userId,
                title: post.title,
                bodyPreview: post.body.truncate(length: previewLenght),
                body: post.body)
        }
    }
}
