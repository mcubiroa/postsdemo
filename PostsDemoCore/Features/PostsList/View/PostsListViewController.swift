//
//  PostsListViewController.swift
//  PostsDemoCore
//
//  Created by Marc Cubiro Aguilar on 31/01/2020.
//  Copyright © 2020 marc.cubiro. All rights reserved.
//

import UIKit

internal protocol PostsListViewProtocol: AnyObject {
    func showListOfPosts(_ posts: [Post])
    func showError(error: ServiceError)
}

// MARK: - PostsListViewController
internal class PostsListViewController: UIViewController, PostsListViewProtocol {
    
    // MARK: - IBOutlets
    @IBOutlet weak var postsTableView: UITableView!
    
    // MARK: - Properties
    let cellIdentifier = "TitleBodyCell"
    
    // MARK: - Variables
    var presenter: PostsListPresenterProtocol!
    var postsList: [Post] = []
    
    // MARK: - Initialization
    init(presenter: PostsListPresenterProtocol) {
        super.init(nibName: String(describing: PostsListViewController.self), bundle: Bundle(for: type(of: self)))
        
        self.presenter = presenter
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Lifecycle methods
    override public func viewDidLoad() {
        super.viewDidLoad()

        presenter.view = self
        presenter.loadView()
        setUpUI()
    }
    
    // MARK: - Setup
    private func setUpUI() {
        title = "core_posts_list_title".localized
        postsTableView.accessibilityIdentifier = "PostsTableView"
        postsTableView.dataSource = self
        postsTableView.delegate = self
        postsTableView.register(UINib(nibName: String(describing: TitleBodyTableViewCell.self), bundle: Bundle(for: type(of: self))), forCellReuseIdentifier: cellIdentifier)
    }

    // MARK: - Internal methods
    internal func showListOfPosts(_ posts: [Post]) {
        DispatchQueue.main.async {
            self.postsList = posts
            self.postsTableView.reloadData()
        }
    }
    
    internal func showError(error: ServiceError) {
        DispatchQueue.main.async {
            let alert = UIAlertController(title: "core_error_title".localized, message: error.description, preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "core_error_retry".localized, style: .default, handler: { action in
                self.presenter.retry()
            }))
            self.present(alert, animated: true)
        }
    }
}

// MARK: - UITableViewDataSource
extension PostsListViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return postsList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath)
        let post = postsList[indexPath.row]
        
        if let postCell = cell as? TitleBodyTableViewCell {
            postCell.bind(title: post.title, body: post.bodyPreview)
        }
        
        return cell
    }
}

// MARK: - UITableViewDelegate
extension PostsListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let post = postsList[indexPath.row]
        presenter.postSelected(post: post)    
    }
}
