//
//  File.swift
//  PostsDemoCore
//
//  Created by Marc Cubiro Aguilar on 31/01/2020.
//  Copyright © 2020 marc.cubiro. All rights reserved.
//

import Foundation

public struct Post: Equatable {
    var id: Int
    var userId: Int
    var title: String
    var bodyPreview: String
    var body: String
}

internal struct PostsResponse: Codable {
    var id: Int
    var userId: Int
    var title: String
    var body: String
}
