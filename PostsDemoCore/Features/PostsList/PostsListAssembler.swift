//
//  PostsListAssembler.swift
//  PostsDemoCore
//
//  Created by Marc Cubiro Aguilar on 31/01/2020.
//  Copyright © 2020 marc.cubiro. All rights reserved.
//

import UIKit

// MARK: - PostsListAssembler
public final class PostsListAssembler {
    
    private var webService: WebService
    private var navigationController: UINavigationController
    
    // MARK: - Initialization
    public init(webService: WebService, navigationController: UINavigationController) {
        self.webService = webService
        self.navigationController = navigationController
    }
    
    // MARK: - View Controller
    public func viewController() -> UIViewController {
        return PostsListViewController(presenter: presenter())
    }
    
    // MARK: - Internal mehtods
    internal func presenter() -> PostsListPresenterProtocol {
        return PostsListPresenter(
            repository: repository(),
            detailAssembler: PostDetailAssembler(
                webService: webService,
                navigationController: navigationController
        ))
    }
    
    internal func repository() -> PostsListRespositoryProtocol {
        return PostsListRepository(webService: webService)
    }
    
}
