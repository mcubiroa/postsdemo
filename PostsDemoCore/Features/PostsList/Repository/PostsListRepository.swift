//
//  PostsListRepository.swift
//  PostsDemoCore
//
//  Created by Marc Cubiro Aguilar on 31/01/2020.
//  Copyright © 2020 marc.cubiro. All rights reserved.
//

import Foundation

internal protocol PostsListRespositoryProtocol {
    func fetchPosts(completion: @escaping (Result<[PostsResponse], ServiceError>) -> Void)
}

internal final class PostsListRepository: PostsListRespositoryProtocol {
    
    internal var webService: WebService
    
    init(webService: WebService) {
        self.webService = webService
    }
    
    internal func fetchPosts(completion: @escaping (Result<[PostsResponse], ServiceError>) -> Void) {
        webService.load([PostsResponse].self, endpoint: .posts) { (response) in
            switch response {
            case .success(let postsList):
                completion(.success(postsList))
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
    
}
