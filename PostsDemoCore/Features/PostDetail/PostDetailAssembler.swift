//
//  PostDetailAssembler.swift
//  PostsDemoCore
//
//  Created by Marc Cubiro Aguilar on 01/02/2020.
//  Copyright © 2020 marc.cubiro. All rights reserved.
//

import UIKit

 // MARK: - PostsListAssembler
public final class PostDetailAssembler {
    
    private var webService: WebService
    private var navigationController: UINavigationController
    
    // MARK: - Initialization
    public init(webService: WebService, navigationController: UINavigationController) {
        self.webService = webService
        self.navigationController = navigationController
    }
    
    // MARK: - View Controller
    public func navigateToPostDetail(post: Post) {
        navigationController.pushViewController(
            PostDetailViewController(presenter: presenter(post: post)),
            animated: true
        )
    }
    
    // MARK: - Internal mehtods
    internal func presenter(post: Post) -> PostDetailPresenterProtocol {
        return PostDetailPresenter(repository: repository(), post: post)
    }
    
    internal func repository() -> PostDetailRespositoryProtocol {
        return PostDetailRepository(webService: webService)
    }
    
}
