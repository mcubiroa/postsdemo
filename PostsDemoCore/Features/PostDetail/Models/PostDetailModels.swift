//
//  PostDetailModels.swift
//  PostsDemoCore
//
//  Created by Marc Cubiro Aguilar on 01/02/2020.
//  Copyright © 2020 marc.cubiro. All rights reserved.
//

import Foundation

public struct PostDTO {
    var id: Int
    var userId: Int
    var title: String
    var body: String
}

internal struct PostDetail: Equatable {
    var id: Int
    var userId: Int
    var title: String
    var body: String
    var comments: [Comment]
}

internal struct Comment: Equatable {
    var id: Int
    var name: String
    var email: String
    var body: String
}

internal struct CommentResponse: Codable {
    var postId: Int
    var id: Int
    var name: String
    var email: String
    var body: String
}
