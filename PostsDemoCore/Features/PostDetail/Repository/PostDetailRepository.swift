//
//  PostDetailRepository.swift
//  PostsDemoCore
//
//  Created by Marc Cubiro Aguilar on 01/02/2020.
//  Copyright © 2020 marc.cubiro. All rights reserved.
//

import Foundation

// MARK: - PostsDescriptionRespositoryProtocol
internal protocol PostDetailRespositoryProtocol {
    func fetchComments(postId: Int, completion: @escaping (Result<[CommentResponse], ServiceError>) -> Void)
}

// MARK: - PostsDescriptionRespositoryProtocol
internal final class PostDetailRepository: PostDetailRespositoryProtocol {

    // MARK: - Variables
    internal var webService: WebService
    
    // MARK: - Initializers
    init(webService: WebService) {
        self.webService = webService
    }
    
    // MARK: - Internal methods
    internal func fetchComments(postId: Int, completion: @escaping (Result<[CommentResponse], ServiceError>) -> Void) {
        webService.load([CommentResponse].self, endpoint: .comments(postId: postId)) { (response) in
            switch response {
            case .success(let postsList):
                completion(.success(postsList))
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
}
