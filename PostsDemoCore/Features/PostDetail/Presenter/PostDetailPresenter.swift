//
//  PostDetailPresenter.swift
//  PostsDemoCore
//
//  Created by Marc Cubiro Aguilar on 01/02/2020.
//  Copyright © 2020 marc.cubiro. All rights reserved.
//

import Foundation

// MARK: - PostsListPresenterProtocol
internal protocol PostDetailPresenterProtocol {
    var view: PostDetailViewProtocol? { get set }
    
    func loadView()
    func retry()
}

// MARK: - PostsListPresenter
internal final class PostDetailPresenter: PostDetailPresenterProtocol {
    
    // MARK: - Properties
    
    // MARK: - Variables
    var repository: PostDetailRespositoryProtocol
    var postDetail: PostDetail!
    weak var view: PostDetailViewProtocol?

    // MARK: - Initialization
    init(repository: PostDetailRespositoryProtocol, post: Post) {
        self.repository = repository
        self.postDetail = getPostDetail(from: post)
    }
    
    // MARK: - Internal methods
    internal func loadView() {
        fetchComments()
        view?.showPost(post: postDetail)
    }
    
    internal func retry() {
        fetchComments()
    }
    
    internal func fetchComments() {
        repository.fetchComments(postId: postDetail.id) { (response) in
            switch response {
            case .success(let commentsList):
                self.view?.showListOfComments(self.getComments(from: commentsList))
            case .failure(let error):
                self.view?.showError(error: error)
            }
        }
    }
    
    internal func getComments(from commentsResponse: [CommentResponse]) -> [Comment] {
        return commentsResponse.map { (comment) -> Comment in
            return Comment(
                id: comment.id,
                name: comment.name,
                email: comment.email,
                body: comment.body
            )
        }
    }
    
    internal func getPostDetail(from post: Post) -> PostDetail{
        return PostDetail(
            id: post.id,
            userId: post.userId,
            title: post.title,
            body: post.body,
            comments: []
        )
    }
}
