//
//  PostDetailViewController.swift
//  PostsDemoCore
//
//  Created by Marc Cubiro Aguilar on 01/02/2020.
//  Copyright © 2020 marc.cubiro. All rights reserved.
//

import UIKit

internal protocol PostDetailViewProtocol: AnyObject {
    func showPost(post: PostDetail)
    func showListOfComments(_ comments: [Comment])
    func showError(error: ServiceError)
}

// MARK: - PostDetailViewController
class PostDetailViewController: UIViewController, PostDetailViewProtocol {
    
    // MARK: - IBOutlets
    @IBOutlet weak var postTitleLabel: UILabel!
    @IBOutlet weak var postBodyLabel: UILabel!
    @IBOutlet weak var commentsLabel: UILabel!
    @IBOutlet weak var commentsTableView: UITableView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    // MARK: - Properties
    let cellIdentifier = "TitleBodyCell"
    
    // MARK: - Variables
    var presenter: PostDetailPresenterProtocol!
    var comments: [Comment] = []
    
    // MARK: - Initialization
    init(presenter: PostDetailPresenterProtocol) {
        super.init(nibName: String(describing: PostDetailViewController.self), bundle: Bundle(for: type(of: self)))
        
        self.presenter = presenter
        self.presenter.view = self
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Lifecycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        presenter.loadView()
        setUpUI()
    }
    
    private func setUpUI() {
        activityIndicator.hidesWhenStopped = true
        activityIndicator.startAnimating()
        commentsLabel.text = "core_posts_detail_comments".localized
        commentsTableView.dataSource = self
        commentsTableView.isHidden = true
        commentsTableView.register(UINib(nibName: String(describing: TitleBodyTableViewCell.self), bundle: Bundle(for: type(of: self))), forCellReuseIdentifier: cellIdentifier)
    }

    // MARK: - Internal methods
    internal func showPost(post: PostDetail) {
        title = post.title
        postTitleLabel.text = post.title
        postBodyLabel.text = post.body
    }
    
    internal func showListOfComments(_ comments: [Comment]) {
        DispatchQueue.main.async {
            self.comments = comments
            self.activityIndicator.stopAnimating()
            self.commentsTableView.isHidden = false
            self.commentsTableView.reloadData()
        }
    }

    internal func showError(error: ServiceError) {
        DispatchQueue.main.async {
            let alert = UIAlertController(title: "core_error_title".localized, message: error.description, preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "core_error_retry".localized, style: .default, handler: { action in
                self.presenter.retry()
            }))
            self.present(alert, animated: true)
        }
    }
}

extension PostDetailViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return comments.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath)
        let comment = comments[indexPath.row]
        
        if let postCell = cell as? TitleBodyTableViewCell {
            postCell.bind(title: comment.name, body: comment.body)
        }
        
        return cell
    }
}
