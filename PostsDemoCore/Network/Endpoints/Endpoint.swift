//
//  Endpoint.swift
//  PostsDemoCore
//
//  Created by Marc Cubiro Aguilar on 31/01/2020.
//  Copyright © 2020 marc.cubiro. All rights reserved.
//

import Foundation

public enum HTTPMethod: String {
    case GET
    case POST
}

public enum Endpoint {
    case posts
    case comments(postId: Int)
}
extension Endpoint {
    public var httpMehtod: HTTPMethod {
        switch self {
        case .posts, .comments:
            return .GET
        }
    }
    
    public var path: String {
        switch self {
        case .posts:
            return "posts"
        case .comments:
            return "comments"
        }
    }
    
    public var parameters: [String: String] {
        switch self {
        case .posts:
            return [:]
        case .comments(let postId):
            return ["postId": "\(postId)"]
        }
    }
}

extension Endpoint {
    func request(with baseURL: URL) -> URLRequest {
        let url = baseURL.appendingPathComponent(path)
        
        var newParameters = parameters
        parameters.forEach { newParameters.updateValue($1, forKey: $0) }
        
        var components = URLComponents(url: url, resolvingAgainstBaseURL: false)!
        if parameters.count > 0  {
            components.queryItems = newParameters.map(URLQueryItem.init)
        }
        
        var request = URLRequest(url: components.url!)
        request.httpMethod = httpMehtod.rawValue
        
        return request
    }
}
