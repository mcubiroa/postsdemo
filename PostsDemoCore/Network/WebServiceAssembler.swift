//
//  WebServiceAssembler.swift
//  PostsDemoCore
//
//  Created by Marc Cubiro Aguilar on 31/01/2020.
//  Copyright © 2020 marc.cubiro. All rights reserved.
//

import Foundation

public final class WebServiceAssembler {
    
    public var webService: WebService = WebService(session: URLSession(configuration: .default))
    
    public init() {}
}
