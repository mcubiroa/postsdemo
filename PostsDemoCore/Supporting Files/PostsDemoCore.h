//
//  PostsDemoCore.h
//  PostsDemoCore
//
//  Created by Marc Cubiro Aguilar on 31/01/2020.
//  Copyright © 2020 marc.cubiro. All rights reserved.
//

#import <Foundation/Foundation.h>

//! Project version number for PostsDemoCore.
FOUNDATION_EXPORT double PostsDemoCoreVersionNumber;

//! Project version string for PostsDemoCore.
FOUNDATION_EXPORT const unsigned char PostsDemoCoreVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <PostsDemoCore/PublicHeader.h>


