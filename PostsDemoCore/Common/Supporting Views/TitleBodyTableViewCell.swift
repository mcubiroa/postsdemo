//
//  TitleBodyTableViewCell.swift
//  PostsDemoCore
//
//  Created by Marc Cubiro Aguilar on 31/01/2020.
//  Copyright © 2020 marc.cubiro. All rights reserved.
//

import UIKit

class TitleBodyTableViewCell: UITableViewCell {

    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var bodyLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        setUpUI()
    }
    
    internal func setUpUI() {
        cardView.layer.cornerRadius = 15
    }
    
    internal func bind(title: String, body: String) {
        titleLabel.text = title
        bodyLabel.text = body
    }
}
