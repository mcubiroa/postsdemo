//
//  Encode+JSON.swift
//  PostsDemoCore
//
//  Created by Marc Cubiro Aguilar on 31/01/2020.
//  Copyright © 2020 marc.cubiro. All rights reserved.
//

import Foundation

extension Encodable {
    func toData() -> Data? {
        return try? JSONEncoder().encode(self)
    }
}
