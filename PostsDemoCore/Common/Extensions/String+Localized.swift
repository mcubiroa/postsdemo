//
//  String+Localized.swift
//  PostsDemoCore
//
//  Created by Marc Cubiro Aguilar on 02/02/2020.
//  Copyright © 2020 marc.cubiro. All rights reserved.
//

import Foundation

extension String {
    var localized: String {
        return NSLocalizedString(self, tableName: "Localizable", bundle: Bundle(identifier: "marc.cubiro.PostsDemoCore") ?? .main, value: "**\(self)**", comment: "")
    }
}
