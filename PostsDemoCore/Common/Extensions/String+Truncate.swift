//
//  String+Truncate.swift
//  PostsDemoCore
//
//  Created by Marc Cubiro Aguilar on 01/02/2020.
//  Copyright © 2020 marc.cubiro. All rights reserved.
//

import Foundation

extension String {
    func truncate(length: Int) -> String {
        if self.count > length {
            return String(self.prefix(length)) + "..."
        } else {
            return self
        }
    }
}
