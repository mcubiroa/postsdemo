# Posts Demo App

PostsDenmo es una app de estilo master-detail en la que se usa la API JSONPlaceholder para mostrar un listado de posts. En la pantalla de detalle se muestra información del post y un listado de comentarios.  
  
Para crear la app se ha usado el patrón MVP con algunas modificaciones para que sea más  fácil de testear y para dividir las responsabilidades de cada capa. A las capas de modelo vista y presenter, se le ha añadido una capa repository que se encarga de proporcionar los datos al presenter y un Assembler que es el encargado de crear el modulo proporcionándole todas las dependencias.  
  
Finalmete se han hecho tests de todos los módulos hasta llegar al 90% de coverage.

### Light mode

![picture](Screenshots/postListLight.png) ![picture](Screenshots/postDetailLight.png)

### Dark mode

![picture](Screenshots/postListDark.png) ![picture](Screenshots/postDetailDark.png)